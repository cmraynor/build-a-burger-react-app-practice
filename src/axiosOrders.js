import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://build-a-burger-16374.firebaseio.com/'
});

export default instance;
