
import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary'
import DeliverySummary from '../../containers/DeliverySummary/DeliverySummary';

class Checkout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ingredients: {},
            price: 0,
        }
    }

    componentDidMount() {
        const query = new URLSearchParams(this.props.location.search);
        const ingredients = {};
        let price = 0;
        for ( let param of query.entries() ) {
            if ( param[0] === 'price' ) {
                price = param[1];
            } else {
                ingredients[param[0]] = +param[1];
            }
        }
        this.setState({ingredients: ingredients, price: price });
    }
   
    checkoutCancel = () => {
        this.props.history.goBack();
    }

    checkoutContinue = () => {
        this.props.history.replace('/checkout/delivery');
    }

    render() {
        return(
            <div>
                <CheckoutSummary 
                    ingredients={this.state.ingredients}
                    checkoutContinue={this.checkoutContinue}
                    checkoutCancel={this.checkoutCancel} />
                <Route 
                    path='/checkout/delivery'
                    render= {(props) =>( <DeliverySummary ingredients={this.state.ingredients} price={this.state.price} {...props}/> ) }/>
            </div>         
        );
    }
}

export default Checkout;