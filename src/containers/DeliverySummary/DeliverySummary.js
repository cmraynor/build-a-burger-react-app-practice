import React, {Component} from 'react';
import Button from '../../components/UI/Button/Button';
import Classes from '../DeliverySummary/DeliverySummary.css';
import axios from '../../axiosOrders';
import Spinner from '../../components/UI/Spinner/Spinner';

class DeliverySummary extends Component {
    state = {
        name: '',
        email: '',
        address: {
            street: '',
            zipCode: '',
        }
    }

    orderHandler = (event) => {
        event.preventDefault();
          this.setState({ loading: true });
          const order = {
              ingredients: this.props.ingredients,
              price: this.props.price,
              customer: {
                  name: 'Sally Random',
                  address: {
                      street: '123 Test St.',
                      zipCode: '123456',
                      country: 'US'
                  },
                  email:'test@test.com'
              },
              deliverMethod: 'fastest'
          }
          axios.post('/orders.json', order )
          .then( response => {
              this.setState({loading: false, ordered: false});
              this.props.history.push('/');
          })    
          .catch( error => {
            this.setState({loading: false, order: false});
          });
    }

    render() {
        let form = (<form>
            <input type="text" name="name" placeholder="Your Name" />
            <input type="email" name="email" placeholder="Your e-mail" />
            <input type="text" name="street" placeholder="Street" />
            <input type="text" name="zip" placeholder="Zip code" />
            <Button btnType="Success" clicked={this.orderHandler}>ORDER</Button>
        </form>);
        if ( this.state.loading ) {
            form = <Spinner />
        }
        return (
            <div className={Classes.DeliveryInfo}>
                <h4>DELIVERY INFO</h4>
                {form}
            </div>
        );
    }

}
export default DeliverySummary;