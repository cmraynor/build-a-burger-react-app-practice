import React, { Component } from 'react';
import Aux from '../../hoc/Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axiosOrders';

const INGREDIENT_PRICES = {
    lettuce: 0.03,
    tomato: 0.03,
    onion: 0.03,
    cheese: 0.4,
    meat: 1.3, 
    bacon: 0.7,
}

class BurgerBuilder extends Component {
    
    state = {
        ingredients: null,
        totalPrice: 4,
        purchaseable: false,
        ordered: false,
        loading: false,
    }
    
    componentDidMount() {
        axios.get('/ingredients.json') 
        .then( response => {
            this.setState({ingredients: response.data });
        })
    }

    addIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type];
        const updatedCount = oldCount + 1;
        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = updatedCount;
        const priceAddition = INGREDIENT_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAddition;
        this.setState({ ingredients: updatedIngredients, totalPrice: newPrice });
        this.updatePurchaseState(updatedIngredients);
      };
      
      removeIngredientHandler = (type) => {
          const oldCount = this.state.ingredients[type];
          if( oldCount <= 0 ) {
              return;
          }
          const updatedCount = oldCount - 1;
          const updatedIngredients = {
              ...this.state.ingredients
          }
          updatedIngredients[type] = updatedCount;
          const priceSubtraction = INGREDIENT_PRICES[type];
          const oldPrice = this.state.totalPrice;
          const newPrice = oldPrice - priceSubtraction;
          this.setState({ ingredients: updatedIngredients, totalPrice: newPrice });
          this.updatePurchaseState(updatedIngredients);
      };

      updatePurchaseState = ( ingredients ) => {
          const sum = Object.keys(ingredients).map( igKey => {
            return ingredients[igKey];
          })
          .reduce( (sum, el) => {
            return sum + el;
          }, 0);

          this.setState({purchaseable: sum > 0 });
      }

      purchaseHandler = () => {
          this.setState({ordered: true});
      }

      purchaseCancelHandler = () => {
        this.props.history.replace('/');
        this.setState({ordered: false});
      }

      purchaseContinueHandler = () => {
        const queryParams = [];
        queryParams.push('price=' + this.state.totalPrice);
        for ( let i in this.state.ingredients ) {
            queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i]));
        }
        const queryString = queryParams.join('&');
        this.props.history.push( {
              pathname: '/checkout',
              search: '?' + queryString,
        });
      }

    render() {
        const disabledInfo = {
            ...this.state.ingredients
        }
        for( let key in disabledInfo ) {
            disabledInfo[key] = disabledInfo[key] <= 0;
        }
        let orderSummary = null;
        let burger = <Spinner />

        if ( this.state.ingredients !== null ) {
            burger =( 
             <Aux> 
                <Burger ingredients={this.state.ingredients} />
                <BuildControls
                    ingredientAdded={this.addIngredientHandler}
                    ingredientRemoved={this.removeIngredientHandler}
                    disabled={disabledInfo}
                    purchaseable={this.state.purchaseable}
                    price={this.state.totalPrice}
                    ordered={this.purchaseHandler}
                />
            </Aux> 
          );
          
          orderSummary = <OrderSummary 
            ingredients={this.state.ingredients}
            canceled={this.purchaseCancelHandler}
            continue={this.purchaseContinueHandler}
            price={this.state.totalPrice} />
        }

        if ( this.state.loading ) {
            orderSummary = <Spinner />
        }
        return (
            <Aux>
                <Modal show={this.state.ordered} modalClosed={this.purchaseCancelHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </Aux>
        );
    }
}

export default withErrorHandler( BurgerBuilder, axios );