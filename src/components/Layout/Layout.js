import React, {Component} from 'react';
import Aux from '../../hoc/Aux';
import classes from './Layout.css';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation/Sidedrawer/Sidedrawer';

class layout extends Component {
    state = {
        sideDrawer: false,
    }

    sideDrawerHandler = () => {
        this.state.sideDrawer ? this.setState({sideDrawer: false}) : this.setState({sideDrawer: true});
    }
    
    render() {
        return (
            <Aux>
            <div> 
                <Toolbar sideDrawer={this.sideDrawerHandler} />
                <SideDrawer hidden={this.state.sideDrawer} />
            </div>
            <main classes={classes.Content}> 
                <h1>BUILD IT YOUR WAY</h1>
                {this.props.children} 
            </main>
        </Aux> 
        );
    }
}    


export default layout;