import React from 'react';
import classes from '../NavigationItems/NavigationItems.css';
import NavigationItem from './NavigationItem/NavigationItem';
import Aux from '../../../hoc/Aux';
 
const navigationItems = () => ( 
    <Aux>
        <ul className={classes.NavigationItems} >
            <NavigationItem link="/" active>Burger Builder</NavigationItem>
            <NavigationItem link="/orders">Your Orders</NavigationItem>
        </ul>
    </Aux>
);

export default navigationItems;