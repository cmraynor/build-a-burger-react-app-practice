import React from 'react';
import classes from './Toolbar.css';
import Logo from '../Logo/Logo';

const toolbar = (props) => {
  return (
          <header className={classes.Toolbar}>
               <Logo />
               <div className={classes.MenuIcon} onClick={props.sideDrawer}>
                    <div className={classes.Line}></div>
                    <div className={classes.Line}></div>
                    <div className={classes.Line}></div>
               </div>
          </header>
     );
}

export default toolbar;