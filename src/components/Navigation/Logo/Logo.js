import React from 'react';
import {withRouter} from 'react-router-dom';
import classes from './Logo.css';
import Logo from '../../../assets/logo.png';
import Button from '../../UI/Button/Button';

const logo = (props) => (
    <Button btnType="Home" clicked={()=>{props.history.push('/')}}>
        <img src={Logo} className={classes.Logo} alt="Build-a-Burger"/>
    </Button>    
);
export default withRouter(logo);