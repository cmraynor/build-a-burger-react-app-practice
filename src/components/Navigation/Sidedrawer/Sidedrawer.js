import React from 'react';
import NavigationItems from '../NavigationItems/NavigationsItems';
import classes from '../../Navigation/Sidedrawer/Sidedrawer.css';

const sideDrawer = (props) => {
    return(
        props.hidden ? <div className={classes.SideDrawer}>
            <nav>
                <NavigationItems />
            </nav>
        </div> : null
    );
};

export default sideDrawer;