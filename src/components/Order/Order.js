
import React from 'react';
import Classes from '../Order/Order.css';
import BurgerIngredient from '../../components/Burger/BurgerIngredient/BurgerIngredient';

const order = (props) => {
    let transformedIngredients = Object.keys( props.ingredients )
    .map( igKey => {
        return [...Array(props.ingredients[igKey])].map((_, i ) => {
            return <BurgerIngredient key={igKey + i} type={igKey} />;
        });
    })
    .reduce( (arr, el ) => {
         return arr.concat(el);
    }, []);
    return (
        <div className={Classes.Order}>
            <BurgerIngredient type="bread-top" />
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom" />
            <p>Price: <strong>${Number.parseFloat( props.price ).toFixed(2)}</strong></p>
        </div>
    ) };

export default order;