import React from 'react';
import classes from '../BuildControls/BuildControls.css';
import BuildControl from './BuildControl/BuildControl';

const controls = [
    { label: 'Lettuce',  type: 'lettuce' },
    { label: 'Tomato',  type: 'tomato' },
    { label: 'Onion',  type: 'onion' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Bacon',  type: 'bacon' },
    { label: 'Beef Patty',  type: 'meat' },
];

const BuildControls = (props) => {
    return (
    <div className={classes.BuildControls}>
         <h2>Current Price: 
             <span className={classes.Price}> ${props.price.toFixed(2)}</span>
         </h2>
        {controls.map( ctrl => (
            <BuildControl 
                key={ctrl.label} 
                label={ctrl.label}
                added={() => props.ingredientAdded(ctrl.type) } 
                removed={() => props.ingredientRemoved(ctrl.type)}
                disabled={props.disabled[ctrl.type]}
               />  
        ))}
        <button 
            className={classes.OrderButton}
            disabled={!props.purchaseable}
            onClick={props.ordered}
            >ORDER NOW</button> 
    </div>
);}

export default BuildControls;