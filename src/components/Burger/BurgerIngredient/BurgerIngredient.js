import React, { Component }from 'react';
import PropTypes from 'prop-types';
import classes from './BurgerIngredient.css';
import breadTop from '../../../assets/breadTop';
import breadBottom from '../../../assets/breadBottom';
import lettuce from '../../../assets/lettuce';
import cheese from '../../../assets/cheese';
import bacon from '../../../assets/bacon';
import meat from '../../../assets/hamburger';
import tomato from '../../../assets/tomatoes';
import onion from '../../../assets/onion';

class BurgerIngredient extends Component {
    render() {
        let ingredient = null;
        switch( this.props.type ) {
            case('bread-bottom'):
                ingredient = <img src={breadBottom} alt="" className={classes.BreadBottom} />;
                break;
            case('bread-top'):
                ingredient = <img src={breadTop} alt="" className={classes.BreadTop} />  
                break;
            case('meat'):  
                ingredient = <img src={meat} alt="" className={classes.BeefPatty} />  
                break;
            case('lettuce'):  
                ingredient = <img src={lettuce} alt="" className={classes.Lettuce} />
                break;
            case('cheese'):  
                ingredient = <img src={cheese} alt="" className={classes.Cheese} />
                break;
            case('bacon'):  
                ingredient = <img src={bacon} alt="" className={classes.Bacon} />
                break;
            case('tomato'):  
                ingredient = <img src={tomato} alt="" className={classes.Tomato} />
                break;
            case('onion'):  
                ingredient = <img src={onion} alt="" className={classes.Onion} />
                break;
             default: 
             break;   
        }
        return ingredient;
    }    
}

BurgerIngredient.propTypes = {
    type: PropTypes.string.isRequired
}

export default BurgerIngredient;