import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import classes from './OrderSummary.css';
import Button from '../../UI/Button/Button';


class orderSummary extends Component {
    render() { 
        const ingredientSummary = Object.keys(this.props.ingredients)
    .map( ( igKey, i ) => {
        return <li key={i}>{igKey}: {this.props.ingredients[igKey]}</li>
    });
    return (
        <div className={classes.OrderSummary}>
            <h3>Your Order</h3>
            <p> A delicious burger with the following ingredients:</p>
            <ul>
                {ingredientSummary}
            </ul>
            <p className={classes.Price}>Total Price: <span className={classes.Cost}>${this.props.price.toFixed(2)}</span></p>
            <p>Continue to Checkout?</p>
            <Button btnType="Danger" clicked={this.props.canceled}>CANCEL</Button>
            <Button btnType="Success" clicked={this.props.continue}>CONTINUE</Button>
        </div>
    );
    }
}

export default withRouter( orderSummary );