# Build-a-Burger - React App Practice

This is a small React app that I am working on connecting to the database and using to explore Redux. 

![](https://lh3.googleusercontent.com/eHgZxevwlFhzBURmHYGDZFmePOAbyYaPpUN47TJweYHdxs1OvdHExU-iS0fV3BPE9VUkfOlam7eDs7fp2iDvkD7iZYILO2RYc6vdyMcgXmJhvaPU8Ayl6QnyBPac-b0lE7D71K2y_phzfvKp19OD1uUsC2rVazqdM7hEoKTSM91-OU-QJpILGB9sc-gHNrLz2DyjFG934shnIlcVRrWKowIvTh33HgipUNXOBs3zhb5Fk3w_-EENnBCG3zAxxLprqEaUVbCCG8kIG7hbsqB8NbNAWs16CFGY3Okqd2zKT7hbVkUOP7EDa9v6lFHLzR7IyoEYKXpE0VBUc7lhZIW0utvCBQIu9YmGyjgQSYt1b5KtCKhAKoxTAHG7aMHGs-cPSTIg_ZPBCOeimtXmllC_Aw99W88SYtQSnRK3L5lw2D4l-_oabbstpPdwDh1pB1dcddZz6nPz9St46MXudPGv07umiBq-NFjt9afnyRpfzBVUyvT4x1FVgdb_EIsX9v-29fflqYIYHL1FLvFeXnuQZDiHmWDYSsg4L42d1Vb_z6CKTjUl0iHYR-u8xlqStrRvxxjufqZtlmaAsBCYhTA78jigHHCaELJa6ENto3Ht8Cmo3fBPn00In3FnAQIk4Q935LQVqttP3vg-Sa8f7pKY-dgFhctczhtwxViVJvwPu6uWcpfTFVo0H4A=w1252-h702-no)

Basic scaffolding and structure adapted from udemy course - 
https://www.udemy.com/course/react-the-complete-guide-incl-redux/learn/lecture/8156730#content